const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 3000;

// app.use(express.static(__dirname + "/../client/"));
// app.use(express.static(path.join(__dirname, "../client")));

app.use(express.static(__dirname));

app.get("/", function (req, res) {
    var html = '<!DOCTYPE html><html lang="en"><head><title>Random Image</title></head><body>';
    var i = Math.floor((Math.random() * 10) + 1);
    html += '<img height="600" src="' + "img/" + i  + '.jpg">';
    html += "</body></html>";
    res.contentType("text/html");
    res.status(200).send(html);
});

app.get("/image", function (req, res) {  
    res.contentType("image/jpeg");
    var i = Math.floor((Math.random() * 10) + 1);
    var imageURL = "img/" + i + ".jpg";
    res.sendFile(path.join(__dirname, imageURL));
});

// Handle 404 - Keep this as a last route
app.use(function(req, res, next) {
    // res.status(400);
    // res.status(404).send('404: File Not Found');
    var html = '<!DOCTYPE html><html lang="en"><head><title>Random Image</title></head><body>';
    html += '<img height="600" src="img/404.png">';
    html += "</body></html>";
    res.status(404).send(html);
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app