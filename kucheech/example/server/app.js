const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const NODE_PORT = process.env.PORT || 3000;

// app.use(express.static(__dirname + "/../client/"));
app.use(express.static(path.join(__dirname, "../client")));

app.get("/hello", function (req, res) {
    res.status(200).send("hello back");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});

//make the app public. In this case, make it available for the testing platform
module.exports = app