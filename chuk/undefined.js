var greet = function(name) {
    if(!name) {
        name = "Alice";
    }
    console.log("Hello %s", name);
}

greet();

greet("Bob");